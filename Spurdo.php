<?php
// Joaquin-V/libspurdo - libspurdo for PHP.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2017 Joaquín Varela

// Credit to <https://github.com/onodera-punpun/bin/blob/master/spurdo>
// for providing the replacements and ebin faces.

namespace JoaquinV\libspurdo;

class libspurdo{

	const REPLACEMENTS = [
		'america'   => 'clapistan',
		'capitalism'=> 'clapistan',
		'communism' => 'gomunesm',
		'meme'      => 'maymay',
		'some'      => 'sum',
		'epic'      => 'ebin',
		'kek'       => 'geg',
		'right'     => 'rite',
		'love'      => 'loab',
		'money'     => 'dolar',

		'ing' => 'ign',
		'alk' => 'olk',

		'ex' => 'egz',
		'ng' => 'nk',
		'ic' => 'ig',
		'ys' => 'yz',
		'ws' => 'wz',
		'us' => 'uz',
		'ts' => 'tz',
		'ss' => 'sz',
		'rs' => 'rz',
		'ns' => 'nz',
		'ms' => 'mz',
		'ls' => 'lz',
		'is' => 'iz',
		'gs' => 'gz',
		'fs' => 'fz',
		'es' => 'ez',
		'ds' => 'dz',
		'bs' => 'bz',
		'tr' => 'dr',
		'ts' => 'dz',
		'pr' => 'br',
		'nt' => 'dn',
		'lt' => 'ld',
		'kn' => 'gn',
		'cr' => 'gr',
		'ck' => 'gg',
		'va' => 'ba',
		'up' => 'ub',
		'pi' => 'bi',
		'pe' => 'be',
		'po' => 'bo',
		'ot' => 'od',
		'op' => 'ob',
		'nt' => 'nd',
		'ke' => 'ge',
		'iv' => 'ib',
		'et' => 'ed',
		'ev' => 'eb',
		'co' => 'go',
		'ck' => 'gg',
		'ca' => 'ga',
		'ap' => 'ab',
		'af' => 'ab',
		'az' => 'ez',
		'ov' => 'ob',
		'av' => 'eb',
		
		'th' => 'd',
		'mm' => 'm',
		'wh' => 'w',
		'll' => 'l',
		
		't' => 'd',
		'k' => 'g',
		
		
	];

	const EBIN_FACES = [
		':D', ':DD', ':DDD', ':-D',
		'XD', 'XXD', 'XDD', 'XXDD',
		'xD', 'xDD', ':dd', 'DDD:',
		'Dx', 'DDx', 'DDX', 'DX'
	];

	public static function spurdo(string $text): string{
		$text = strtolower($text);
		foreach(self::REPLACEMENTS as $orig => $spur)
			$text = str_replace($orig, $spur, $text);

		while(preg_match('/\.|,|\?|!(?=\s|$|\.)/m', $text)){
			$face = self::EBIN_FACES[array_rand(self::EBIN_FACES)];
			$text = preg_replace('/\.|,|\?|!(?=\s|$|\.)/m',
				sprintf(' %s', $face), $text, 1);
		}

		return $text;
	}
}
